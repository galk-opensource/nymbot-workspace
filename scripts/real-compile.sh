## this script compiles program for arena. recieve input or - [program folder name] - or - "all"


if [ $# -eq 0 ]
  	then
    echo "No arguments recieved"
    exit
    
fi

# Use ENV variable as the numbot folder path if initiazted. otherwise default path is Home.
if [[ -z ${NYMBOT_PROJECT} ]]; then
	NYMBOT_PROJECT="${HOME}"
else
	NYMBOT_PROJECT="${NYMBOT_PROJECT}"
fi

cd "${NYMBOT_PROJECT}/nymbot-workspace"
echo "Entered ${NYMBOT_PROJECT}/nymbot-workspace"
echo "target program:$1"
## DPROG sends varaible PROG to the called cmake.(CMakeLists in lib). same thing to DBUILD.
if [ $1 == "all" ]
	then
	cmd="cmake -S ../../lib -DBUILD=REAL -DPROG=ALL"
elif  [ -d "programs/$1" ]
	then 
	cmd="cmake -S ../../lib -DBUILD=REAL -DPROG=$1"
else
	echo "program not exist"
    exit
fi

echo $cmd

mkdir -p realbin&& cd realbin
rm -rf build_files
mkdir -p build_files && cd build_files
eval $cmd
make
cd ..
echo "Done. To run please execute"
# In the local machine it was origanlly the following command,but changed to work under the assumption the 4 respotires nymbot package is in a top folder
# echo "~/nymbot-workspace/realbin/$1 0 '-c' '/home/bot/Desktop/aruco-detector/config/calibration/DFK_33UX/DFK_ux33_calib.yml' '-config' '/home/bot/Desktop/aruco-detector/config/ARUCO_MIP_16h3_conf.yml'"
echo "~/nymbot-workspace/realbin/$1 0 -c ../nymbot-tracker/config/calibration/DFK_33UX/DFK_ux33_calib.yml -config ../nymbot-tracker/config/ARUCO_MIP_16h3_conf.yml"
exit



