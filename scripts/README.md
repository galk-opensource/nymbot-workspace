# how the compilation works
##real-compile.sh
the row in real-compile.sh:
```
cmd="cmake -S ../../lib -DBUILD=REAL -DPROG=ALL"
```
makes a makefile and the needed files for compilation using the CMakeLists files in the lib folder.

the main CMakeLists file in lib sets variables for the CMakeLists files in the folders in lib.
those CMakeLists files make the makefile itself in folder realbin/build_files

**important for adding linking variables**
in the CMakeFiles in the folders in lib there are "target_link_libraries" and "link_directories". those variables tell the linker where to search for the libraries.
if you get an error of format
'''
undefined reference to `X'
'''
and the referance is not to one of the libraries that are in the SWARM project or opencv,its because the linker doesnt find them.
to add the linkage paths to the functions of the unreferenced libraries,add them to "target_link_libraries" and "link_directories".
i had this problem with the curses library and it fixed it(you can see curses is added to list of variables in "target_link_libraries" and "link_directories".)

# bugs
when running 
```
./scripts/real-compile.sh all
```
after line
```
Building CXX object realbots/CMakeFiles/selected_nymbot_calibration.dir/home/USER/nymbot-workspace/robot-utility/selected_nymbot_calibration/selected_nymbot_calibration.cpp.o
```
getting error 
'''
*** No rule to make target '/home/USER/nymbot-tracker/lib/libDetector.a', needed by '/home/USER/nymbot-workspace/realbin/selected_nymbot_calibration'.  Stop.
'''

the bug happens in the make stage ("make" line) in real-compile.sh,after running CMakeLists.txt with cmake.(eval $cmd). there are 2 CMakeLists.txt,one in lib and one in lib/realbots.
some of the pathes in those CMakeLists files require nymbot-tracker(and nymbot-infrastructure) files. 
**They must be compiled first**

**the fix: follow the steps in nymbot-tracker README.md for how to compile it("cmake ." and "make").**

**same problem and fix for nymbot-infrastructure(from the README):**
```
1. Clone the repository
2. Compile the program ("make") in the main folder, nymbot-infrastructure
3. Enter the lib folder by executing "cd lib" in the terminal
4. Install the program by executing "make install" in the terminal(if no premission,run with sudo)
```
## /usr/bin/ld: cannot find -l<library>: No such file or directory

library is missing. install it.

### -lcurses,curses library
[https://en.wikipedia.org/wiki/Ncurses]
Used in program "realtime".
Install by the command 
```
sudo apt-get install libncurses5-dev libncursesw5-dev
```


