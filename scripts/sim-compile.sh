## this script compiles program for simulator. recieve input or - [program folder name] - or - "all"
## note: need to create argos file for each experiment in path "nymbot-workspace/include/sim_exp"

if [ $# -eq 0 ]
  	then
    echo "No arguments recieved"
    exit
fi

cd ~/nymbot-workspace
echo $1
if [ $1 == "all" ]
	then
	cmd="cmake -S ../../lib -DBUILD=SIM -DPROG=ALL"
elif  [ -d "programs/$1" ]
	then 
	cmd="cmake -S ../../lib -DBUILD=SIM -DPROG=$1"
else
	echo "program not exist"
    exit
fi

echo $cmd
mkdir -p simbin&& cd simbin
rm -rf build_files
mkdir -p build_files && cd build_files
eval $cmd
make
cd ..
cd ..

file_path=$(pwd)
file_path+="/include/sim_exp/insectbot_messy_"
file_path+=$1
file_path+="_exp.argos"


echo "Done. To run please execute"
echo "argos3 -c $file_path"
exit

