## Documentation

### users

[doc](https://docs.google.com/document/d/11IeJWTPqbRCnptF5mSeR5-sueknnhl3PrWypZBzBnB8/edit?usp=sharing])

### developers

[doc](https://docs.google.com/document/d/1khkK7KXMERmsUXVVnhOwyUG6aESkG1PuoBgDdT1iwB4/edit)

### Other docs
[*Important if using the real robots*] Explanation about how the real robots work are under `/docs/how-the-real-robots-logic-chain-works.md`<br> 
Compilation explaintion and bugs are under `/scripts/README.md`

## Before you start
__if you freshly downloaded/cloned the nymbot package,make sure to do the following steps:__

1. read the README.md of nymbot-infrastructure and compile the folder.
2. read the README.md of nymbot-tracker and compile the folder + make sure opencv is downloaded
3. read the above docs file.



### __Setting the nymbots project path for the CMakeLists with ENV variable NYMBOT_PROJECT.__

The CMakeLists of the nymbot-workspace need a path to the folder in which all the nymbot projects are. this path is used in the compile process.

You can set an enviorment variable yourself or not touch anything,keeping the default nymbot projects path,Home on linux.

__How to set the enviorment variable for your own use:__
1. Open terminal in your Home folder.
2. Open the file .bashrc with the command `open .bashrc`. by default,file is hidden.
3. Add the line `export NYMBOT_PROJECT = "<nymbot project full path(starting from Home)"`.<br>In this folder should be all your "nymbot-*" folders.
4. Save.



## Bugs

### __*GStreamer warning: Cannot query video position: status=0, value=-1, duration=-1*__

When running scripts output for example:
```
~/nymbot-workspace/realbin/$1 0 '-c' '../nymbot-tracker/config/calibration/DFK_33UX/DFK_ux33_calib.yml' '-config' '../nymbot-tracker/config/ARUCO_MIP_16h3_conf.yml'
```

You get the message
```
[ WARN:0@0.919] global /home/<user>/opencv-master/modules/videoio/src/cap_gstreamer.cpp (1412) open OpenCV | GStreamer warning: Cannot query video position: status=0, value=-1, duration=-1
```
the internet says its a problem with opencv class CvCapture_GStreamer in cap_gstreamer.cpp.  

*the program is supposed to work even with this message,ignore it*  

the github ticket on this issue: [github](https://github.com/opencv/opencv/issues/10324)


### __Error 13 from open: Permission denied__

*The first quick solution* is to add the user to the dialout group as written in the "nymbot-infracture" README:
```
sudo usermod -a -G dialout <username>
```

Check if the user has been added to the group after logging out and in:
```
getent group dialout
```

If your user is in the list and your premission is still denied,its because you didn't fully logged out and in(logging out with the linux interface doesn't work for some reason).

*two solutions*:
1. 
log out by the command:
```
su <username>
```
More about solution: [askubuntu](https://askubuntu.com/questions/455000/group-permissions-allow-but-still-get-permission-denied)

This only works on the current terminal

2.  *Beware: kills all user procceses. save everything brfore running*
the following command will log you out and go to the login screen
```
loginctl terminate-user <username>
```
This way no procceses are reminded, which ensure that logging in doesn't copy an old login,which prevents updating the groups.
More about solution: [askubuntu](https://askubuntu.com/questions/1045993/after-adding-a-group-logoutlogin-is-not-enough-in-18-04)

### __[ERROR:0@0.001] global <opencv download path> (505) open Can't open file: '~/nymbot-tracker/config/calibration/DFK_33UX/DFK_ux33_calib.yml' in read mode__

After running a program with the command:
```
<program path> 0 [-c cameraParams.yml] [-config configFile.yml]
```

looks like it happens because the class doesn't find the yml files paths when [-c cameraParams.yml] and [-config configFile.yml] are wrapped up by `' '`. 

remove them.

