#include <stdint.h>
#include <iostream>
#include <math.h>

#include <nymbots.h>
#include "wave.h"
using namespace std;


template <class T>
bool equal (T a1, T a2) {
    return fabs(a1 - a2) < 0.05;
}


void setState(TStateNames state, int index) {
	nymbots_arr[index].state= state;
}

bool allInPlace(){
	for (int index = 0; index<ROBOT_MAX_NUMBER; index++){
		nymbot_location_data nymbot = nymbots_arr[index];
        	float reqPos = startPositionY + index * interval;
		if (!(equal(nymbot.x,startPositionX) && equal(reqPos,nymbot.y) && nymbot.state == INSECTBOT_STATE_STOP)) {
			cout<<nymbot.x<<","<<nymbot.y<<endl;
			cout<<startPositionX<<","<<reqPos<<endl;
			cout<<"state: "<<nymbot.state<<endl;
			cout<<"not in place!!"<<endl;
            return false;
        }
    }
    return true;
}

int fixAngle(double angle){
	if (angle > wantedAngle+threshold)
		return -2;
	if (angle < wantedAngle-threshold)
		return 2;
	return 0;

}



void control_step() {
for (int index = 0; index<ROBOT_MAX_NUMBER; index++){
	update_velocity(index, forwardSpeed , 10);
}

/*

	static int loops = 0;
    _LastSeenPosition();
    
    static int inPos=-1;
    if (inPos != 1){
	inPos = allInPlace();
		if (inPos != 1) {
		//	return;
		}}
    // already fix places
    static int isMoving = -1;
    isMoving = allInPlace();
    if (loops%20 ==0){
    	for (int index = 0; index<ROBOT_MAX_NUMBER; index++){
		nymbot_location_data nymbot = nymbots_arr[index];

		double angle =nymbot.orientation;

    	//int angVel = fixAngle(angle);
		int last_index = (ROBOT_MAX_NUMBER + (index-1)% ROBOT_MAX_NUMBER)% ROBOT_MAX_NUMBER;

		nymbot_location_data last_nymbot = nymbots_arr[last_index];

		TStateNames currState = nymbot.state;
		cout<<currState<<endl;



		if (isMoving || (currState == INSECTBOT_STATE_STOP && last_nymbot.state == INSECTBOT_STATE_MOVING)){ // if its the first or 
			setState(INSECTBOT_STATE_MOVING,index);
		   	update_velocity(index, forwardSpeed , 0);
			break;
		} 
		else if (currState == INSECTBOT_STATE_MOVING  && nymbot.x <0 ){
			setState(INSECTBOT_STATE_TURNING,index);
			update_velocity(index, backwardSpeed , 0);
			break;
		} 
		else if ((currState == INSECTBOT_STATE_TURNING) && (nymbot.x >startPositionX)){
			update_velocity(index, noSpeed , 0);
			setState(INSECTBOT_STATE_STOP,index);
			break;
		}
		else {
		    	int speed = noSpeed;
		    	if (currState ==  INSECTBOT_STATE_TURNING) {
				speed = backwardSpeed;
		    	} else if (currState ==  INSECTBOT_STATE_MOVING) {
				speed = forwardSpeed;
		    	}
		   	update_velocity(index,speed, 0);
		}
	  }
	}**/
   // loops++;
}




