#ifndef CW_CIRCLE_H
#define CW_CIRCLE_H

#include <stdlib.h>
#include <nymbots.h>

template <class T>

/*** inner functions, no need to declare ****/
bool equal (T a1, T a2);
bool allInPlace();
int fixAngle(double angle);

#define MAX_X_FOR_VECTOR 100

static float calculate_error(int);
/* THIS IS A CLOCKWISE PROGRAM!!! */

static int calculate_desired_orientation(int);
static int calculate_desired_distance_from_center_arena(int);
static void calculate_nymbot_sight(int, int dir);
static bool nymbot_within_desired(int id, int dir);
static int correct_direction(int id);
static int vertical_center, horizantal_center;


#endif





