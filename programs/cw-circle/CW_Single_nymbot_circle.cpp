#include "CW_Single_nymbot_circle.h"
#include <stdint.h>
using namespace std;
#include <vector>
#include <math.h>

#define OUT_CIRCLE 0 
#define TO_CIRCLE 1
#define TO_CENTER 2
#define PI 3.14159265
#define clockwise 1
#define counter_clockwise -1 

// started to think about counter clockwise circle, but calculating wantedOrientation not finished yet.
// clockwise circle is working good. 

static int dir = clockwise;


void fix_hard(int id) {
    nymbots_arr[id].current_linear_speed = noSpeed + forwardSpeed*0.3;
    nymbots_arr[id].current_angular_speed = dir*2;
}
void fix_light(int id) {
    nymbots_arr[id].current_linear_speed = noSpeed + forwardSpeed*0.5 ;
    nymbots_arr[id].current_angular_speed = dir*1;

}
void fix_light_opp(int id) {
    nymbots_arr[id].current_linear_speed = noSpeed + forwardSpeed*0.5 ;
    nymbots_arr[id].current_angular_speed = dir*-1;
}
void no_fix(int id) {
    nymbots_arr[id].current_linear_speed = noSpeed + forwardSpeed*0.7 ;
    nymbots_arr[id].current_angular_speed = 0;
}

double distance(Point p1, Point p2) {
    double deltaX = p1.x - p2.x;
    double powX = pow(deltaX,2);
    double deltaY = p1.y - p2.y;
    double powY = pow(deltaY,2);
    double dist = sqrt(powX + pow(deltaY,2));
    return dist;
}


static int counter = 1;
Point center;
void control_step()
{
    if (counter == 1) {
        center.x = float(get_center().x);
        center.y = float(get_center().y);
    }
    _LastSeenPosition();
    counter++;
    for (int id =0; id<ROBOT_MAX_NUMBER;id++) {

        nymbot_location_data nymbot = nymbots_arr[id];
        Point location = {nymbot.x, nymbot.y};
        double dist =distance(location,Point{center.x, center.y}); 
        nymbots_arr[id].dist_from_center = dist;
        nymbots_arr[id].wantedOrientation = calculate_desired_orientation(id);   
  
        bool ahead_out_circle = nymbot_within_desired(id, OUT_CIRCLE);
        bool ahead_to_circle = nymbot_within_desired(id, TO_CIRCLE);
        bool ahead_center = nymbot_within_desired(id, TO_CENTER);
 


        if (inner_radius > nymbots_arr[id].dist_from_center)  {
            if (ahead_center) {
                fix_light_opp(id);
            } else {
                no_fix(id);
            }

        }
        else if (outer_radius < nymbots_arr[id].dist_from_center){
            if (ahead_to_circle) {
                fix_light(id);
            } else if (ahead_center) {
                fix_light_opp(id);
            } else {
                 fix_hard(id);
            }
        }
        else  {
            if (ahead_to_circle) {
                no_fix(id);
            } else if (ahead_out_circle) {
                fix_light(id);
            } else if (ahead_center) {
                fix_light_opp(id);
            } else {
                fix_hard(id);
            }
        
        }
      update_velocity(id, nymbots_arr[id].current_linear_speed,nymbots_arr[id].current_angular_speed);
    }
}


static int calculate_desired_orientation(int nymbotId)
{
    nymbot_location_data nym_data = nymbots_arr[nymbotId];
    float angle ;
    float deltaX = nym_data.x - center.x;
    float deltaY = nym_data.y - center.y;

    if (!deltaX) {
        angle =  0;
    }   else if (!deltaY) {
        angle =  180;
    }   else {
        float rad = atan(-deltaX/deltaY);
        angle = int(rad * 57.2957795)%360;

    }
    if (deltaY>0) {
        angle = angle + 180;
    }
   return (int((dir*angle)+360))%360; 
}

static int calculate_desired_distance_from_center_arena(int nymbotId)
{
    float free_space = outer_radius - inner_radius;
    float partial = (free_space / ROBOT_MAX_NUMBER) ;
    return (inner_radius + ((0.5 + nymbotId) * partial));
}

static void calculate_nymbot_sight(int nymbotId, int direction) //
{
    int right_threshold;
    int left_threshold;
    if ((direction == TO_CIRCLE && direction> 0) ||(direction == OUT_CIRCLE && direction < 0)) {
        right_threshold = 0;
        left_threshold = 40;
    } else if ((direction == OUT_CIRCLE&& direction> 0) || (direction == TO_CIRCLE && direction< 0)){
        right_threshold = -40;
        left_threshold = 0;
    } else if (direction == TO_CENTER) {
            right_threshold = 40;
            left_threshold = 120;
    }
    int* sight_arr = nymbots_arr[nymbotId].nymbot_sight;
    int orientation = nymbots_arr[nymbotId].wantedOrientation;
    int val1 = (orientation + right_threshold + 360) % 360;
    int val2 = (orientation + left_threshold + 360) % 360;

    *sight_arr = val1;
    *(sight_arr+1) = val2;
}

static bool nymbot_within_desired(int id,int dir)
{
    calculate_nymbot_sight(id,dir);
    nymbot_location_data nymbot = nymbots_arr[id];
    int min = nymbot.nymbot_sight[0];
    int max = nymbot.nymbot_sight[1];
    
    int orientation = nymbot.orientation;
    bool ans;

    if (min < max) {
        ans = (min < orientation && orientation < max);
    } else {
        
        ans =  (min < orientation || orientation < max);
    }
    return ans; 
}
