#include <stdint.h>
#include <iostream>
#include <math.h>

#include <nymbots.h>
#include "realtime.h"
#include <curses.h>
using namespace std;

#define UP 56
#define DOWN 50
#define LEFT 52
#define RIGHT 54
#define NYMBOTNUM 6



template <class T>
bool equal (T a1, T a2) {
    return fabs(a1 - a2) < 0.05;
}


void move(char key)
{
	static int speed = 0;
	static int ang = 0;
	cout<<"key = "<<key<<endl;
    switch(key)
    {
	    case UP:
	    {
	        cout << "Up" << endl;//key up
			speed = forwardSpeed;
			ang = 0;
	        break;
	    }
	    case DOWN:
	    {
	        cout << endl << "Down" <<endl << endl;   // key down
			speed = backwardSpeed;
			ang = 0;
	        break;
	    }
	    case RIGHT:
	    {
	        cout << "Right" <<endl << endl;  // key right
			speed = forwardSpeed* 0.5;
			ang = 2;
	        break;
	    }
	    case LEFT:
	    {
	        cout << "Left" << endl <<endl;  // key left
			speed = forwardSpeed* 0.5;
			ang = -2;
	        break;
	    }
		default:{
			cout << "NULL" <<endl;	// not arrow
			//speed = nymbots_arr[NYMBOTNUM].speed;
			//ang = nymbots_arr[NYMBOTNUM].angular_velocity;
			break; 
		}
		
	}
	//nymbots_arr[NYMBOTNUM].speed = speed;
	//nymbots_arr[NYMBOTNUM].angular_velocity = ang;
	update_velocity(NYMBOTNUM, speed , ang);
 
}


void control_step() {
	for (int index = 0; index<ROBOT_MAX_NUMBER; index++){
		initscr();
	  	timeout(-1);
	  	int c = getch();
	  	endwin();	

		move(c);
		
		//update_velocity(index, forwardSpeed , 10);
	}
}




