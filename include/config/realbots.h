#ifndef CONSTS
#define CONSTS

#define PATH "/home/mor/nymbot-tracker/"
#define EXP "app/"
#define EXE "./scripts/real-compile.sh"


#define temp "realbin/template.so 0 3 '-c' '/home/bot/Desktop/aruco-detector/config/calibration/DFK_33UX/DFK_ux33_calib.yml' '-config' '/home/bot/Desktop/aruco-detector/config ARUCO_MIP_16h3_conf.yml'"




#define MIN_SPEED 0
#define MAX_ANGULAR_VELOCITY 3
#define MIN_ANGULAR_VELOCITY -3
#define startPositionX 0.35f
#define startPositionY -0.4f
#define interval 20
#define forwardSpeed 500
#define backwardSpeed -30
#define noSpeed 0
// 0 is stop, forward speed < 0

#define threshold 3
#define wantedAngle 180


#define CORRIDOR_WIDTH 350
#define DEFAULT_NYMBOT_LINEAR_SPEED 320
#define DEFAULT_NYMBOT_ANGULAR_SPEED 0

#define outer_radius 1000
#define inner_radius 700




#endif
