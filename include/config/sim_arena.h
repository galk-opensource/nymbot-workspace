#define centerX 0
#define centerY 0

#define topRightX 0.5f
#define topRightY 0.5f

#define topLeftX -0.5f
#define topLeftY 0.5f

#define bottomRightX 0.5f
#define bottomRightY -0.5f

#define bottomLeftX -0.5f
#define bottomLeftY -0.5f



/*
 * NOTICE the values can be changed due to physical move of the arena this is only for reference
     
	bottomRight										bottomLeft
 (0.5, -0.5) - - - - - - - - - - - - - - - - - - -  (-0.5, -0.5)
            |                                     |
            |   360<-                    ^270|0   |
            |                                     |
	 	  	|             = = = = =               |
			|          =            =             |
		    |        =                =           |
            |        =  	(0, 0)	   =          |
            |        =                =           |
            |          =             =            |
            |   |          = = = = =              |
    upRight |  \/ 90                     ->180    |	upLeft
    (0.5,0.5) - - - - - - - - - - - - - - - - - - - (-0.5,0.5)
                                  				
*/



