#define centerX 2509
#define centerY 1804

#define topRightX 943
#define topRightY 90

#define topLeftX 812
#define topLeftY 3509

#define bottomRightX 4305
#define bottomRightY 199

#define bottomLeftX 4176
#define bottomLeftY 3578



/*
 * NOTICE the values can be changed due to physical move of the arena this is only for reference
     
  bottomRight	 									bottomLeft
 (4305, 199) - - - - - - - - - - - - - - - - - - -  (4176, 3578)
            |                                     | 
            |   360<-                    ^270|0   |
            |                                     |
		    |             = = = = =               |
			|          =            =             |
		    |        =                =           |
            |        =  (2509, 1804)   =          |
            |        =                =           |
            |          =             =            |
            |  ||          = = = = =              |
    upRight |  \/ 90                     ->180    |	upLeft
    (943,90) - - - - - - - - - - - - - - - - - - - (812, 3509)
                               					   UP
*/



