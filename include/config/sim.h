
#define ROBOT_MAX_NUMBER 1

#define MAX_SPEED 8
#define MIN_SPEED -8
#define MAX_ANGULAR_VELOCITY 3 // left
#define NO_ANGULAR_VELOCITY 0	// no turn
#define MIN_ANGULAR_VELOCITY -3 // right
#define startPositionX 0.35f
#define startPositionY -0.4f
#define interval 0.3f
#define forwardSpeed 100
#define backwardSpeed -100
#define noSpeed 100
#define threshold 3
#define wantedAngle 180


#define DEFAULT_ANGULAR_VELOCITY 5
#define DEFAULT_SPEED 148

#define TOP_RIGHT_CORNER_X -0.5
#define TOP_RIGHT_CORNER_Y -0.5

#define TOP_LEFT_CORNER_X 0.5
#define TOP_LEFT_CORNER_Y -0.5

#define BOTTOM_RIGHT_CORNER_X -0.5
#define BOTTOM_RIGHT_CORNER_Y 0.5

#define BOTTOM_LEFT_CORNER_X 0.5
#define BOTTOM_LEFT_CORNER_Y 0.5


#define outer_radius 0.3
#define inner_radius 0.15




