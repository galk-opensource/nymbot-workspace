
#if defined(SIM)
#include <sim-adapter.h>
#elif defined(REALBOTS)
#include <realbots-adapter.h>

#endif

#include <../lib/include/point.h>

void control_step();
extern void _LastSeenPosition();
extern Point get_center();
extern int norm_angle(int angle);
extern void update_velocity(int id, int speed, int angular_vel);




