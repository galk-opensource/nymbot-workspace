// you may change this structure for program need, add or delete fields. but it must contain x,y and orientation.


#ifndef NYMBOT_LOCATION_DATA_H
#define NYMBOT_LOCATION_DATA_H

using namespace std;

#include <stdint.h>
#include <vector>


enum TStateNames {INSECTBOT_STATE_STOP, INSECTBOT_STATE_TURNING, INSECTBOT_STATE_MOVING};

struct nymbot_location_data {
    float x;
    float y;
    double orientation;
    double wantedOrientation;
    uint16_t angular_velocity = 0;
    vector<float> speed {0.0,0.0};
    bool is_managed = false;

    int32_t  current_linear_speed = 0; // for now its between 355 to -355
    int32_t  current_angular_speed = 0; // for now its between -15 to 15
    int nymbot_sight[2];
    TStateNames state;
    double dist_from_center;
    bool valid = true;
};


#endif
