#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <cctype>
#include <random>
#include "../../include/config/sim.h"
using namespace std;

 //TODO: check for existing file
 //TODO: check for valid start position(between walls and other nymbots)
float random_position()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dist(-0.5, 0.5);
 
    return dist(gen);
}

string generate(int index) {
		string insectbot = "\t <insectbot id='kb";
		insectbot.append(to_string(index));
		insectbot.append("'> <controller config='1' /> <body position='");
		insectbot.append(to_string(random_position()));
		insectbot.append(",");
		insectbot.append(to_string(random_position()));
		insectbot.append(",0' orientation='");
		insectbot.append(to_string(rand() % 360));
		insectbot.append(",0,0' /> </insectbot>");
		return insectbot;
}

int main(int argc, char **argv)
{
	std::ifstream file("./lib/sim/insectbot_messy_template_exp.argos");
	 if (!file.is_open()) {
	 	cout<<"Error in reading template file"<<endl;
	 	exit;
	 }

	
	ofstream out_file (argv[1]);
	std::string line;
	string wordToReplace("XXX");
    	string wordToReplaceWith(argv[2]);
    	size_t len = wordToReplace.length();
    	
	while (std::getline(file, line)) {
		size_t pos = line.find(wordToReplace); // replace XXX with program so file
		if (pos != string::npos){  
			line.replace(pos, len, wordToReplaceWith);
		}
		
		size_t arena_pos = line.find("</arena>"); // enter insectbot before end of arena configurarion
		if (arena_pos != string::npos){
			for (int i=0; i<ROBOT_MAX_NUMBER ;i++){
				out_file<<generate(i)<<endl;
			}
		}
		out_file<<line<<endl;
	}
	file.close();
	out_file.close();

}


