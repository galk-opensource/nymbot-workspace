#include <sim-adapter.h>

CInsectbotTemplate* simulator;
extern Point arena_center;


void Adapter::adapterOnFrame(){
	control_step();
}

void Adapter::init(CInsectbotTemplate* sim){
	simulator = sim;
}

void _LastSeenPosition() {
	simulator->LastSeenPosition();
}

void update_velocity(int id, int speed, int angularVel){
	simulator->nymbot_update_velocity(id,speed,angularVel);
}

Point get_center(){
	return Point(0.5,0.5);
}

int norm_angle(int angle){
	return angle;
//	return ((angle * -1)+360)%360;
}

