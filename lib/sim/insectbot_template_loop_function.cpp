
#define __template_extern
#include <insectbot_template_loop_function.h>

using namespace std;

Adapter adapter;

CInsectbotTemplate::CInsectbotTemplate(){};

/*
 * 0 < speed < 200. speed = 100 -> stay in place
 * 0 < angularVel < 15. angularVel = 0 -> move straight
 */
void CInsectbotTemplate::nymbot_update_velocity(int id, int speed, int angularVel) {
    int i = 0;
    CSpace::TMapPerType &m_cInsectbots = GetSpace().GetEntitiesByType("insectbot");
    for (CSpace::TMapPerType::iterator it = m_cInsectbots.begin();
         it != m_cInsectbots.end();
         ++it) {

        if (i == id) {
            CInsectbotEntity &cInsectbots = *any_cast<CInsectbotEntity *>(it->second);
            CDummyNymbot &IController = dynamic_cast<CDummyNymbot &>(cInsectbots.GetControllableEntity().GetController());
            IController.setVelocity(speed, angularVel);
            return;
        }
        i++;
    }
    // no such robot
}

void CInsectbotTemplate::Init(TConfigurationNode& t_node) {
        for (int i=0 ; i<ROBOT_MAX_NUMBER;i++){
        	nymbots_arr[i].state = INSECTBOT_STATE_STOP;
        }
	adapter.init(this);
}



CDegrees getOrientation(CInsectbotEntity &cInsectbots){
    vector<Real> orient;
    CQuaternion cQuat = cInsectbots.GetEmbodiedEntity().GetOriginAnchor().Orientation;
    CRadians cZAngle, cYAngle, cXAngle;
    CDegrees xDegree;
    cQuat.ToEulerAngles(cZAngle, cYAngle, cXAngle);
    xDegree = ToDegrees(cZAngle);
    return xDegree;
}


Real CInsectbotTemplate::normOrientation(CDegrees orientation) {
    Real orientVal = orientation.UnsignedNormalize().GetValue();
    return (((int)orientVal * 1) + 360)%360;
}


void CInsectbotTemplate::LastSeenPosition() {
    int nymbotID = 0;
    CSpace::TMapPerType &m_cInsectbots = GetSpace().GetEntitiesByType("insectbot");
    for (CSpace::TMapPerType::iterator it = m_cInsectbots.begin();
         it != m_cInsectbots.end();
         ++it) {
        CInsectbotEntity &cInsectbots = *any_cast<CInsectbotEntity *>(it->second);
        CDummyNymbot &IController = dynamic_cast<CDummyNymbot &>(cInsectbots.GetControllableEntity().GetController());

        CDegrees orientation = getOrientation(cInsectbots);
        vector<double> l = IController.getLocation();
        nymbots_arr[nymbotID].x = l.at(0);
        nymbots_arr[nymbotID].y = l.at(1);
        nymbots_arr[nymbotID].orientation = normOrientation(orientation);
        nymbotID++;
    }
}

nymbot_location_data* getNymbotArr() {
	return nymbots_arr;
}


void CInsectbotTemplate::PreStep() {
	adapter.adapterOnFrame();
}


REGISTER_LOOP_FUNCTIONS(CInsectbotTemplate, "insectbot_template_loop_function")

