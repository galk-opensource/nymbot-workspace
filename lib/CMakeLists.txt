project(workspace)

#
# Set minimum required version
#
cmake_minimum_required(VERSION 3.0.2)

if(POLICY CMP0042)
  cmake_policy(SET CMP0042 NEW)
endif(POLICY CMP0042)
if(POLICY CMP0054)
  cmake_policy(SET CMP0054 NEW)
endif(POLICY CMP0054)

# *when* gets called with "eval $cmd" from real-compile.sh:
# PROG can be "ALL" or folder name(variable from user when real-compile is called)
# BUILD will always be "REAL"

# set nymbot project path based on if .bashrc have the ENV variable or not.
set(HOME $ENV{HOME})
if(DEFINED ENV{NYMBOT_PROJECT})
	set(NYMBOT_PROJECT $ENV{NYMBOT_PROJECT})
else()
	set(NYMBOT_PROJECT ${HOME})
endif()
message(STATUS "NYMBOT_PROJECT path set to: ${NYMBOT_PROJECT}")
	
set(WS ${NYMBOT_PROJECT}/nymbot-workspace)

if (PROG STREQUAL "ALL") 
	file(GLOB programs ${WS}/programs/*)
else() 
	file(GLOB programs "${WS}/programs/${PROG}")
endif()

if (BUILD STREQUAL "SIM")
	find_package(Lua REQUIRED)
	set (ARGOS ${HOME}/argos3)
	set(SIM ${NYMBOT_PROJECT}/nymbot-simulator)
	set(SIMULATOR_INCLUDE_DIRS ${SIM}/src)
	set(SIMULATOR_LIBRARY_DIRS ${SIM}/build ${SIM}/argos3/build_simulator)
	set(headers ${ARGOS_INCLUDE_DIRS} ${LUA_INCLUDE_DIR})
	set (WS_INCLUDE_DIRS ${WS}/include)
	add_subdirectory(sim)

elseif (BUILD STREQUAL "REAL")
	file(GLOB utilities ${WS}/robot-utility/*)	# TODO: compile all utilitys. need to choose?
	set(TRACKER ${NYMBOT_PROJECT}/nymbot-tracker)
	SET(infrastructure ${NYMBOT_PROJECT}/nymbot-infrastructure)
	add_subdirectory(realbots)

endif()


