#ifndef ADAPTER_H
#define ADAPTER_H

#include <config/sim.h>
#include <config/sim_arena.h>
#include <insectbot_template_loop_function.h>
#include <../lib/include/point.h>

class CInsectbotTemplate;
extern void control_step();
void LastSeenPosition();
void nymbot_update_velocity(int id, int speed, int angularVel);

class Adapter {
	private:
	public:
		void init(CInsectbotTemplate* sim);
		void adapterOnFrame(); 
};



#endif

