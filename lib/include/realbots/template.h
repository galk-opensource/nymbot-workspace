#ifndef TEMPLATE_H
#define TEMPLATE_H

#include <iostream>
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"
extern "C" {
#include <nymbot_transmitter.h>
}

#include <config/realbots.h>
#include <nymbot_location_data.h>
#include <realbots-adapter.h>


#ifndef __template_extern
	#define nld_extern extern
#else
	#define nld_extern
#endif

nld_extern nymbot_location_data nymbots_arr[ROBOT_MAX_NUMBER];


__transmitter_extern T_RES nymbot_update_velocity(int a, int b, int c);
void LastSeenPosition(SceneData& sd);
void OnFrame(SceneData sd);
int main(int argc,char** argv);
void calculate_center_arena(SceneData sd);

#endif
