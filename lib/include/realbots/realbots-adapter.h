#ifndef ADAPTER_H
#define ADAPTER_H

#include <iostream>
#include "AsyncDetector.h"
#include "VideoFile.h"
#include "CmdLineParser.h"
extern "C" {
#include <nymbot_transmitter.h>
}


#include <config/realbots.h>
#include <config/realbots_arena.h>
#include <template.h>
#include <../lib/include/point.h>

extern void control_step();
extern void LastSeenPosition(SceneData& sd);

class Adapter {
	public:
		void adapterOnFrame(SceneData &sd); 
};




#endif
