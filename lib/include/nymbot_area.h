#ifndef NYMBOT_AREA_H
#define NYMBOT_AREA_H


using namespace argos;
using namespace std;

#include "range.h"
#include "line.h"


struct nymbot_area {
    Real right_front[2];
    Real left_front[2];
    Real right_back[2];
    Real left_back[2];
    
    Real origin_right_front[2];
    Real origin_left_front[2];
    Real origin_right_back[2];
    Real origin_left_back[2];
    Range range;
    Line lines[4];
    int updateTime = -1;
};

#endif

