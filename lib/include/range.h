#ifndef RANGE_H
#define RANGE_H


using namespace argos;
using namespace std;



struct Range {
	Real xRange[2];
	Real yRange[2];
	int index;
};

#endif

