#ifndef POINT_H
#define POINT_H


class Point{
    public:
        float x;
        float y;
        Point(float _x,float _y) {
            x = _x;
            y = _y;
        }
        Point() {
            x = 0;
            y = 0;
        }

        Point operator - (Point const &obj) {
			Point res;
			res.x = x - obj.x;
			res.y = y - obj.y;
			return res;
        }
};

#endif
