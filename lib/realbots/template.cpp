
#include <fstream>

#define __template_extern
#include <template.h>

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

Adapter adapter;
cv::Point arena_center;
cv::Mat img;

// pressing ctrl+C will stop all robots
void my_handler(int s){

	cout<<endl<<"stop all nymbots"<<endl;
	for (int i=0; i<ROBOT_MAX_NUMBER;i++) {
		nymbot_update_velocity(i,0,0);
	}
	cout<<endl<<"experiment done"<<endl;
	exit(1); 
}

void LastSeenPosition(SceneData& sd) {
    uint8_t numOfLocusts= sd.Locusts.size();
    for (int i = 0; i < numOfLocusts; ++i) {
        Locust& l = sd.Locusts.at(i);
        if(l.m_Marker.isValid()) {
            nymbots_arr[i].orientation = l.GetOrientation();
            nymbots_arr[i].orientation =((int)nymbots_arr[i].orientation+270)%360;
            nymbots_arr[i].x = l.GetPositionIJ().x;
            nymbots_arr[i].y = l.GetPositionIJ().y;
            nymbots_arr[i].valid = true;
        }
		else{
			nymbot_location_data* nymdata = &nymbots_arr[i];
			nymdata->orientation = USHRT_MAX;
			nymdata->x = INT32_MIN;
			nymdata->y = INT32_MIN;
			nymdata->angular_velocity = 0;
            nymbots_arr[i].valid = false;

		}
    }
}

bool isFirst = true;
int loops =0;

void OnFrame(SceneData sd) {
    img = sd.LastFrame.clone();
    if (isFirst) {
        calculate_center_arena(sd);
        isFirst = false;
    }
	adapter.adapterOnFrame(sd); // This is where control of robots is done.
    loops++;
    cout<<loops<<endl;
}

// calculates center of arena with at least 3 barcodes.
void calculate_center_arena(SceneData sd)
{
    int index[] = {0,0,0,0};
    int def_val = 2000;   
    cv::Point two_right_barcodes[2] = {{def_val,def_val},{def_val,def_val}}, two_left_barcodes[2] = {{def_val,def_val},{def_val,def_val}};
    auto bar_code_list = sd.Locusts;
    cv::Point position;
    //cout << "1) No barcode is visible outside of the ring \n2) No nymbot placed at the corners of the ring\nPress enter to continue" << endl;
    //cin.ignore();

    for (auto& bar_code : bar_code_list)
    {
        if(! bar_code.m_Marker.isValid())
        {
            continue;
        }
        position = bar_code.GetPositionIJ();
        if(position.x > two_right_barcodes[0].x || position.x > two_right_barcodes[1].x)
        {
            if(position.y < two_right_barcodes[0].y)
            {
                index[0] = 1;
                two_right_barcodes[0] = position;
            } else if (position.y > two_right_barcodes[1].y) {
                index[1] = 1;
                two_right_barcodes[1] = position;
            } 
        } else if(position.x < two_left_barcodes[0].x || position.x < two_left_barcodes[1].x)
        {
            if(position.y < two_left_barcodes[0].y)
            {
                index[2] = 1;
                two_left_barcodes[0] = position;
            } else if (position.y > two_left_barcodes[1].y) {
                index[3] = 1;
                two_left_barcodes[1] = position;
            } 
        }
        int x = def_val;
        int y = def_val;
        int sum = index[0] + index[1] + index[2] + index[3];
        if (sum == 4) {
            x  = (two_left_barcodes[0].x + two_left_barcodes[1].x + two_right_barcodes[0].x + two_right_barcodes[1].x) /4;
            y  = (two_left_barcodes[0].y + two_left_barcodes[1].y + two_right_barcodes[0].y + two_right_barcodes[1].y) /4;
        } else if (1 < sum && sum < 4){
            if (two_left_barcodes[0].x != def_val && two_right_barcodes[1].x != def_val) {
                x  = (two_left_barcodes[0].x + two_right_barcodes[1].x) /2;
                y  = (two_left_barcodes[0].y + two_right_barcodes[1].y) /2;
            } else if (two_left_barcodes[1].x != def_val && two_right_barcodes[0].x != def_val) {
                x  = (two_left_barcodes[1].x + two_right_barcodes[0].x) /2;
                y  = (two_left_barcodes[1].y + two_right_barcodes[0].y) /2;
            } else {
                cout<<"not enoght data to calculate center"<<endl;
            }
        }
        arena_center.x = x;
        arena_center.y = y;
        
    }
    
    cout << two_left_barcodes[0] << ", " << two_left_barcodes[1] << "\t" << two_right_barcodes[0] << ", " << two_right_barcodes[1] << endl;
    cout << arena_center.x << ", " << arena_center.y << endl;

}

// main program. working with sync detector.
int main(int argc,char** argv)
{
    CmdLineParser cml(argc, argv);
    string calibCamera;
    string configFile;
    if (argc < 2 || cml["-h"])
    {
        cerr << "Usage: (invideo|cameraindex) [-c cameraParams.yml] [-config configFile.yml]" << std::endl;
        return 0;
    }
    if (cml["-c"])
        calibCamera =cml("-c");
    if (cml["-config"])
        configFile =cml("-config");

    VideoFile* vs;

    try {
        int camIndex = std::stoi(argv[1]);
        vs = new VideoFile(camIndex, calibCamera);
    }
    catch (const cv::Exception e) {
        vs = new VideoFile(argv[1], calibCamera);
    }
    catch (const std::exception& e) {
        vs = new VideoFile(argv[1], calibCamera);
    }
    if (!vs->isOpened()) throw std::runtime_error("Could not open video");
    cv::Size camSize = vs->GetCameraParameters().CamSize;
    bool writeSize = vs->SetProps(cv::CAP_PROP_FRAME_WIDTH, camSize.width);
    writeSize = vs->SetProps(cv::CAP_PROP_FRAME_HEIGHT, camSize.height);

    T_RES res =nymbot_init("/dev/ttyUSB0");
    nymbot_calibrate("<path_to_calib_file>>/calib.txt");

    if(res != T_RES_SUCCESS)
        return -1;    


    struct sigaction sigIntHandler;
	sigIntHandler.sa_handler = my_handler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);


    Detector d(configFile,vs,OnFrame);
    d.Start();

    exit(0);
}



