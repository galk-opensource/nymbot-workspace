#include "realbots-adapter.h"

SceneData sd;
extern cv::Point arena_center;

void Adapter::adapterOnFrame(SceneData &sd1){
	sd = sd1;
	control_step();
}


void _LastSeenPosition() {
	LastSeenPosition(sd);
}

void update_velocity(int id, int speed, int angularVel){
	nymbot_update_velocity(id,speed,angularVel);
}

Point get_center(){
	calculate_center_arena(sd);
	return Point(arena_center.x,arena_center.y);
}

int norm_angle(int angle){
	return (angle+90)%360;
}

