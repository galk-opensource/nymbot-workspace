# TODO for Nymbot Project

1. [ ] robots stable electro-mechanically (uses infrastructure only)
  1. [ ] create procedures for greasing, dusting, ...
  1. [ ] List mechanical tests that we can use to prove robot is OK
    - [ ] List of robots that have **proven** mechanical/electronics problem
  1. [ ] check whether we need to lift/lower/change height of net

2. [ ] robots stable low-level control (uses infra+tracker+ws)
  1. [ ] low-level calibrated control (check existing calibration, make improvements)
    - straight ahead
    - turning

3. [ ] fix arena to work (tracker+simulator+workspace)
  - arena size to be measured in cm --> config to workspace, input to simulator
  - arena orientation calibration using markers --> config to workspace, input to simulator
  - test to check arena is clear, no markers outside of it

4.  [ ] robots control in standard units (uses infra+tracker+ws)
  - cm/s for linear velocity
  - degrees/s for angular velocity
  - simulator uses standard units

5. [ ] Write circle code, other swarm code

6. [ ] find way to work reliably work with 0.7cm markers.
    - 0.5cm preferred


## Repositories for control of physical nymbots

nymbot-infrastructure
nymbot-tracker

## Repositories for simulation of nymbots

nymbot-simulator

## nymbot-workspace  <----  you are here





