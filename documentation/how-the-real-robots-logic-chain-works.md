This documnation is written to give an overall understanding of the logic of the real robots chain. from the command getting out from the pc,tracking the robot and the robot moving.

we have 3 libraries that are part of the chain:

1. nymbot-infrastructure
2. nymbot-tracker
3. nymbot-workplace

## nymbot-infrastructure

Implementation of the communication logic pc->transmitter->robots.
That includes constructing messages and data structures and sending them via bluetooth to the robots chips,which the engines act upon.<br>

the API have 2 layers,high and low. the high layer uses functions of the low level.

### High layer functions - in file `<include/nymbot_transmitter.c>`

1. __nymbot_update_velocity (int id, int speed, int angularVel )__<br>
Is used to change a specific robot speed and degree.<br>
Inputs are: (1) id of robot (2) new robot speed(-375 to 375 in mm/s) (3) new robot angular value(-15 to 15 in degrees/s)

2. __nymbot_calibrate(const char* filePath)__<br>
Is used to SET the calibration of the wheels of each robot.
Input is:<br>
(-) path to a calibration file which specifiy each robot engines power supply. <br>*Calibration file is created by running `nymbot_calibration` in `<nymbot-tracker/bin>`*<br>
OR<br>
(-) no file("") which defaults to regular power supply.<br>
__READ MORE__ in the file "Overview of the nymbot_calibration program"<br><br>
..... add picture of calibration file.....<br>
__why its needed__ - robots max right wheel engine doesn't always have the same engine power as the max left wheel engine,which prevents straight movement - and vice-versa. The calibration slows the stronger wheel to the power of the weaker one, giving expected behavior. <br>


3. __nymbot_dictionary(const char* filePath)__<br>
Is used to SET the dictionary which connects between each robot id and it's barcode in the arena.
*Dictionary file is created by running `barcode_to_id_map` in `<nymbot-tracker/bin>`* <br>
__READ MORE__ in the file "Overview of the nymbot dictionary program"<br><br>
...... add picture of dictionary file.....

### Low layer functions - in file `<include/nymbot_transmitter_low.c>`
    
1. __nymbot_init(const char portName[])__<a name="nymbot_init"></a> <br>
Is used to connect via bluetooth to the robots.<br>
It calls the function Serial_connect from the class `Serial_uart_port_handler.c` which takes care of the connection.<br>
<br>
There are two more functions(nymbot_update_pwm and nymbot_update_normalized_pwm). Read more about them at the [docs section](#infr-docs)

### How to write a program from nymbot-infrastructure only
* assuming you already have calibration and dictionary files or 
1) Call `nymbot_init` to initiate a bluetooth conenction with the robots
2) Call `nymbot_calibrate` to populate the nymbot calibration data structure
3) Call `nymbot_dictionary` to populate the nymbot dictionary data structure
4) Call `nymbot_update_velocity` on specific robot or in a loop on many robots to send movement commands to them.
### Relevent Docs <a name="infr-docs"></a>

... will be updated when all docs are inserted into the project<br>
"A guide for the user in nymbot_infrastructure"<br>

## nymbot-tracker

### config files and variables for functions

Basiclly every function in nymbot-tracker use 2 config files and 1 variable:

### (invideo|cameraindex)
__invideo__ is a path to a saved video of the arena
__cameraindex__ is a value(0,1..) of the index of the wanted camera on the operating system.
in the case of our camera a gstreamer format can also be transferred -
```
"tcambin ! video/x-raw,format=GRAY8,width=5472,height=3648,framerate=18/1 ! videoconvert ! appsink"
```
### cameraParams.yml
Path to a file with parameters for the camera.<br>
Its the output of the camera calibration process as detailed at `<nymbot-tracker/README.md>` under the `Camera Calibration` Section.

Already made cameraParams.yml files are under `<nymbot-tracker/config/calibration>` <br>
for example the file `DFK_ux33_calib.yml` [open file](../../nymbot-tracker/config/calibration/DFK_33UX/DFK_ux33_calib.yml)

### configFile.yml
Path to a file with different parameters - mainly to the Aruco library which is used to detect the markers. <br>
[__READ MORE__ at the config.md doc in nymbot-tracker](../../nymbot-tracker/doc/config.md)

Already made configFile.yml files are under `<nymbot-tracker/config>`

^^^.........path to be changed.........

for example the file ARUCO_MIP_16h3_conf.yml [open file](../../nymbot-tracker//config/ARUCO_MIP_16h3_conf.yml)