## How to compile the ws folder via vsc

1. You need to have the "CMake Tools" extension. download it.
2. Open your `settings.json` file.
3. Add "cmake.sourceDirectory" variable to tell vsc where the "CMakeLists.txt" file is.
For example(and probably what you want): 
```
"cmake.sourceDirectory": "${workspaceFolder}/lib"
```
4. Add "cmake.configureArgs" variable to pass flags with the compile. 
For example:
```
 "cmake.configureArgs": ["-DBUILD=REAL", "-DPROG=ALL"]
```

You can see/use the [`settings.json` file](settings.json) in this folder.